//
//  TweetersTableViewController.swift
//  Stanford Table View Demo
//
//  Created by Mohamed Saeed  on 1/17/18.
//  Copyright © 2018 Mohamed saeed . All rights reserved.
//

import UIKit
import CoreData
class TweetersTableViewController: FetchedResultsTableViewController {
    var hashtag : String?{
        didSet{
            updateUI()
        }
    }
    var container: NSPersistentContainer? = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer
    
    fileprivate var fetchedResultsController: NSFetchedResultsController<TwitterUser>?
    
    func updateUI()
    {
        if  let context = container?.viewContext , hashtag != nil
        {
            let tweeterFetchReq : NSFetchRequest<TwitterUser> = TwitterUser.fetchRequest()
            tweeterFetchReq.predicate = NSPredicate(format: "any tweets.text contains[c] %@", hashtag! )
            tweeterFetchReq.sortDescriptors = [NSSortDescriptor(key: "handle", ascending: true)]
            
            fetchedResultsController = NSFetchedResultsController(fetchRequest: tweeterFetchReq,
                                                                  managedObjectContext: context,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
            try?  fetchedResultsController?.performFetch()
            tableView.reloadData()
        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tweeter", for: indexPath)
        
        if let tweeterUser = fetchedResultsController?.object(at: indexPath){
            
            cell.textLabel?.text = tweeterUser.handle
            cell.detailTextLabel?.text = String (getTweetsCountfor(tweeterUser))
            
        }
        return cell
    }
    
    
    func getTweetsCountfor( _ user: TwitterUser) -> Int {
        let tweetFetchReq: NSFetchRequest<Tweet> = Tweet.fetchRequest()
        tweetFetchReq.predicate = NSPredicate (format: "text contains[c] %@ and tweeter.handle = %@" , hashtag!, user.handle!)
        let count = try? container?.viewContext.count(for: tweetFetchReq) ?? 0
        return count!
    }
}

extension TweetersTableViewController
{
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return fetchedResultsController?.sections?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if let sections = fetchedResultsController?.sections, sections.count > 0 {
            return sections[section].numberOfObjects
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {   if let sections = fetchedResultsController?.sections, sections.count > 0 {
        return sections[section].name
    } else {
        return nil
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]?
    {
        return fetchedResultsController?.sectionIndexTitles
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int
    {
        return fetchedResultsController?.section(forSectionIndexTitle: title, at: index) ?? 0
    }
    
}
