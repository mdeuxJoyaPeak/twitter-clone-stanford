//
//  CoreDataTableViewController.swift
//  Stanford Table View Demo
//
//  Created by MacBook Pro on 1/16/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import Twitter
import CoreData
class CoreDataTableViewController: TableViewController {
    var container : NSPersistentContainer? = (UIApplication.shared.delegate as? AppDelegate )?.persistentContainer
    override func insertTweets (_ newTweets: [Twitter.Tweet])
    {
        super.insertTweets(newTweets)
        updateDatabase(newTweets)
    }
    
    private func  updateDatabase (_ newTweets: [Twitter.Tweet]){
        container?.performBackgroundTask{ [weak self] context in
            for tweet in newTweets
            {
                _ = try? Tweet.findOrCreateTweet(matching: tweet ,in: context)
            }
            
            try? context.save()
            self?.printDatabaseStats()
            
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "tweeters":
             if let tweetersTable =  segue.destination as? TweetersTableViewController{
                tweetersTable.hashtag = searchText
                tweetersTable.container = container
            }
        default:
            break
        }
    }
    func printDatabaseStats()
    {
        if let context = container?.viewContext {
            context.perform {
                
            
            if Thread.current.isMainThread {
                print ("Main thread on" )
            }
            else {
                print ("Main thread off" )

            }
            let request: NSFetchRequest<Tweet> = Tweet.fetchRequest()
            if let tweetCount = (try? context.fetch(request) )?.count{
                print("tweet conunt: \(tweetCount)")
            }
            
            if let tweeterCount = try? context.count(for: TwitterUser.fetchRequest())
            {
                print("user conunt: \(tweeterCount)")
                }
                
            }
        }
    }
}
