//
//  TweetViewCell.swift
//  Stanford Table View Demo
//
//  Created by User on 1/15/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import Twitter
class TweetViewCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var dateCreated: UILabel!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var tweetContent: UILabel!
    
    var tweet: Twitter.Tweet? { didSet { updateUI() } }
    private func updateUI() {
        tweetContent?.text = tweet?.text
        userName?.text = tweet?.user.description
        if let profileImageURL = tweet?.user.profileImageURL {
            // MARK: Fetch data off the main queue
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                if let imageData = try? Data(contentsOf: profileImageURL) {
                    // MARK: UI -> Back to main queue
                    DispatchQueue.main.async {
                        self?.userImg?.image = UIImage(data: imageData)
                    }
                }
            }
        } else {
            userImg?.image = nil
        }
        if let created = tweet?.created {
            let formatter = DateFormatter()
            if Date().timeIntervalSince(created) > 24*60*60 {
                formatter.dateStyle = .short
            } else {
                formatter.timeStyle = .short
            }
            dateCreated?.text = formatter.string(from: created)
        } else {
            dateCreated?.text = nil
        }
        
    }
}
