//
//  TableViewController.swift
//  Stanford Table View Demo
//
//  Created by User on 1/15/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import Twitter
class TableViewController: UITableViewController , UITextFieldDelegate {
    
    var tweets = [Array<Twitter.Tweet>](){
        didSet {
            tableView.reloadData()
           // print(tweets)
        }
    }
    var searchText : String? {
        didSet {
            searchTxtField?.text = searchText
            searchTxtField?.resignFirstResponder()
            tweets.removeAll()
            tableView.reloadData()
            searchForTweets()
            title = searchText
        }
    }
    
  internal func insertTweets (_ newTweets: [Twitter.Tweet])
    {
        tweets.insert(newTweets, at: 0)
    }
    
    func searchForTweets() {
        if let query = searchText {
            Twitter.Request(search: query, count: 15).fetchTweets {[weak self] (fetchedTweets) in
                self?.insertTweets(fetchedTweets)
            }
            
        }
    }
    
    @IBOutlet weak var searchTxtField : UITextField!{
        didSet {
            searchTxtField.delegate = self
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("should return")
        textField.resignFirstResponder()
        if (textField == searchTxtField){
            searchText = searchTxtField.text
            
        }
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchText = "#stanford"
         tableView.rowHeight = UITableViewAutomaticDimension
      //   tableView.estimatedRowHeight = tableView.rowHeight
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return tweets.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tweets[section].count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TweetCell", for: indexPath)
        
        if let tweetCell = cell as? TweetViewCell{
            if  !tweets.isEmpty , !tweets[indexPath.section].isEmpty{
            let curTweet = tweets[indexPath.section][indexPath.row]
                tweetCell.tweet = curTweet
            }
            else {
                print("tweets are empty")
            }
        }
        return cell
    }
}
