//
//  Tweet.swift
//  Stanford Table View Demo
//
//  Created by MacBook Pro on 1/16/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import CoreData
import Twitter
class Tweet: NSManagedObject {

 class func findOrCreateTweet(matching tweetInfo: Twitter.Tweet , in context : NSManagedObjectContext) throws -> Tweet{
        let request: NSFetchRequest<Tweet> = Tweet.fetchRequest()
        request.predicate = NSPredicate(format:"unique = %@" , tweetInfo.id)
        
        do {
        let matches = try context.fetch(request)
        if matches.count > 0 {
            assert(matches.count == 1 , "Tweet.findOrCreateTweet--Database inconsistsy ")
                 return matches[0]
            }
       
        }
        catch{
            throw error
        }
        
        let tweet = Tweet(context: context)
        tweet.created = tweetInfo.created
        tweet.unique = tweetInfo.id
        tweet.text = tweetInfo.description
        tweet.tweeter = try? TwitterUser.findOrCreateUser(userInfo: tweetInfo.user , context: context)
        
        return tweet
    }
}
