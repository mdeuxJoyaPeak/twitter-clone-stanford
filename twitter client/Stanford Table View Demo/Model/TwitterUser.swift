//
//  TweeterUser.swift
//  Stanford Table View Demo
//
//  Created by MacBook Pro on 1/16/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import CoreData
import Twitter
class TwitterUser: NSManagedObject {
    
    class func findOrCreateUser ( userInfo : Twitter.User , context: NSManagedObjectContext ) throws -> TwitterUser
    {
        let request: NSFetchRequest<TwitterUser> = TwitterUser.fetchRequest()
        request.predicate = NSPredicate(format:"handle = %@" , userInfo.screenName)
        
        do {
            let matches = try context.fetch(request)
            if matches.count > 0 {
                assert(matches.count == 1 , "Tweet.findOrCreateTweet--Database inconsistsy ")
                return matches[0]
            }
            
        }
        catch{
            throw error
        }
        
        let twitterUser = TwitterUser(context: context)
        twitterUser.handle = userInfo.screenName
        twitterUser.name = userInfo.name
        
        return twitterUser
    }
}
